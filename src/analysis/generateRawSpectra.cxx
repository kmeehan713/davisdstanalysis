//Extracts the yield of pions plus and minus by doing a simultaneous
//fit to the tof optimized and combined histograms

#include <iostream>
#include <vector>

#include <TString.h>
#include <TFile.h>
#include <TTree.h>
#include <TH1D.h>
#include <TH3D.h>
#include <TF1.h>
#include <TEventList.h>

#include <TCanvas.h>
#include <TSystem.h>
#include <TStyle.h>
#include <TLine.h>
#include <TGraphErrors.h>
#include <TPaveText.h>
#include <TPaveStats.h>
#include <TLegend.h>
#include <TString.h>
#include <Fit/Fitter.h>
#include <Fit/BinData.h>
#include <Fit/Chi2FCN.h>
#include <Math/WrappedMultiTF1.h>
#include <HFitInterface.h>

#include "globalDefinitions.h"
#include "ParticleInfo.h"
#include "UserCuts.h"

#include "utilityFunctions.h"
#include "fitZTPCUtilities.h"
#include "YieldExtractionFit.h"

bool draw = false;

/****************************************/
//Global Control over the length of the spectra
const Double_t MIN_MTM0(0.1);
const Double_t MAX_MTM0(1.5);
//Global Control for where the tof and tpc spectra should be stitched
//This is a suggestion. If the TPC spectrum does not extend this far
//then the tof spectrum is allowed to start earlier.
const Double_t TPC_TOF_STITCH(0.5);
/****************************************/

//Previous Electron Amplitude
Double_t PREVIOUS_ELECTRON_AMP(-1);

//Controls used to stop spectra
Bool_t STOP_TPC_SPECTRUM(false);
Bool_t STOP_TOF_SPECTRUM(false);

//Other Functions in this file
void CreateSpectra(std::vector<TGraphErrors *> &spectraVec, int pid, int charge, unsigned int iCentBin,
		   int detector);

//Double_t ComputeSystematicErrors(CurrentBinInfo *binInfo, Double_t nEvents, Int_t plusOrMinus,
//				 std::vector<std::pair<double,double> > particleParamErrs);
			    
void ScaleTofSpectrum(TGraphErrors *tpcSpectrum, TGraphErrors *tofSpectrum);
void RemovePointsAbove(TGraphErrors *graph, Double_t xVal);
void RemovePointsBelow(TGraphErrors *graph, Double_t xVal);
void ConstructSpectraRatio(TGraphErrors *spectraPlus, TGraphErrors *spectraMinus, TGraphErrors *spectraRatio);

//____MAIN____
void GenerateRawSpectra(TString inputYieldHistoFile, TString outputSpectraFile, TString pidCalibrationFile,
			Int_t userSpecies=-999, Int_t userCentIndex=-999, Int_t userRapidityIndex=-999){

  gStyle->SetOptFit(1);
  gStyle->SetOptStat(0);

  //Create an instance of Particle Info
  ParticleInfo *particleInfo = new ParticleInfo(GetStarLibraryVersion(),true,nRapidityBins,
						pidCalibrationFile);

  //Open the Pid Calibration File so that we can access the tof optimized fits
  TFile *pidFile = new TFile(pidCalibrationFile,"READ");


  //Particles Whose yeilds should be extracted
  std::vector<int> particles;
  if (userSpecies != -999 && userSpecies >=0 && userSpecies <= PROTON)
    particles.push_back(userSpecies);
  else {
    particles.push_back(PION);
    particles.push_back(KAON);
    particles.push_back(PROTON);
  }

  const unsigned int nParticles = particles.size();
  const unsigned int nCentralityBins = GetNCentralityBins();
  
  //Create the Output File
  TFile *outFile = NULL;
  if (outputSpectraFile.CompareTo("")){
    outFile = new TFile(outputSpectraFile,"RECREATE");
    outFile->cd();
    for (unsigned int iParticle=0; iParticle<nParticles; iParticle++){
      outFile->mkdir(Form("RawSpectra_%s",particleInfo->GetParticleName(particles.at(iParticle),1).Data()));
      outFile->mkdir(Form("RawSpectra_%s",particleInfo->GetParticleName(particles.at(iParticle),-1).Data()));
      outFile->mkdir(Form("SpectraRatio_%s",particleInfo->GetParticleName(particles.at(iParticle)).Data()));      
      outFile->mkdir(Form("YieldExtractionFits_%s",particleInfo->GetParticleName(particles.at(iParticle),1).Data()));
      outFile->mkdir(Form("YieldExtractionFits_%s",particleInfo->GetParticleName(particles.at(iParticle),-1).Data()));
      outFile->mkdir(Form("YieldExtractionGraphs_%s",particleInfo->GetParticleName(particles.at(iParticle),1).Data()));
      outFile->mkdir(Form("YieldExtractionGraphs_%s",particleInfo->GetParticleName(particles.at(iParticle),-1).Data()));
      
      outFile->cd(Form("YieldExtractionFits_%s",particleInfo->GetParticleName(particles.at(iParticle),1).Data()));
      for (uint iCentBin = 0; iCentBin < nCentralityBins; iCentBin++)
	gDirectory->mkdir(Form("Cent%02d",iCentBin));
      
      outFile->cd();
      outFile->cd(Form("YieldExtractionFits_%s",particleInfo->GetParticleName(particles.at(iParticle),-1).Data()));
      for (uint iCentBin = 0; iCentBin < nCentralityBins; iCentBin++)
	gDirectory->mkdir(Form("Cent%02d",iCentBin));
      
      outFile->cd();
      outFile->cd(Form("YieldExtractionGraphs_%s",particleInfo->GetParticleName(particles.at(iParticle),1).Data()));
      for (uint iCentBin = 0; iCentBin < nCentralityBins; iCentBin++)
	gDirectory->mkdir(Form("Cent%02d",iCentBin));
      
      outFile->cd();
      outFile->cd(Form("YieldExtractionGraphs_%s",particleInfo->GetParticleName(particles.at(iParticle),-1).Data()));
      for (uint iCentBin = 0; iCentBin < nCentralityBins; iCentBin++)
	gDirectory->mkdir(Form("Cent%02d",iCentBin));
    }
  }
    
  //Create Canvases
  TCanvas *yieldCanvas = NULL;
  TCanvas *spectraCanvas = NULL;
  TCanvas *ratioCanvas = NULL;
  TH1F *spectraFrame = NULL;
  TH1F *ratioFrame = NULL;
  double yMin(.001), yMax(1000);
  if (draw){
    yieldCanvas = new TCanvas("fitCanvas","fitCanvas",20,20,1800,500);
    yieldCanvas->Divide(4,1);
    yieldCanvas->cd(1);
    gPad->SetLogy();
    yieldCanvas->cd(2);
    gPad->SetLogy();
    yieldCanvas->cd(3);
    gPad->SetLogy();
    yieldCanvas->cd(4);
    gPad->SetLogy();
    
    spectraCanvas = new TCanvas("spectraCanvas","spectraCanvas",20,600,800,600);
    spectraCanvas->SetLogy();
        
    ratioCanvas = new TCanvas("ratioCanvas","ratioCanvas",900,600,800,600);

  }
  
  std::vector<TGraphErrors *> spectraPlus(nRapidityBins,(TGraphErrors *)NULL);
  std::vector<TGraphErrors *> spectraMinus(nRapidityBins,(TGraphErrors *)NULL);
  std::vector<TGraphErrors *> spectraRatio(nRapidityBins,(TGraphErrors *)NULL);
  std::vector<TGraphErrors *> spectraPlusTOF(nRapidityBins,(TGraphErrors *)NULL);
  std::vector<TGraphErrors *> spectraMinusTOF(nRapidityBins,(TGraphErrors *)NULL);
  
  //Open the input Yield Histogram file
  TFile *yieldHistoFile = new TFile(inputYieldHistoFile,"READ");

  //Get the Number of Events Histo
  TH1D *nEventsHisto = (TH1D *)yieldHistoFile->Get("nEvents");
  
  //Yield Histograms
  TH3D *zTPCPlusHisto3D, *zTPCMinusHisto3D;
  TH3D *zTOFPlusHisto3D, *zTOFMinusHisto3D;
  
  //Loop Over the Particles, Centrality Bins, Rapidity Bins, and mTm0 Bins and extract the yield
  for (unsigned int iParticle=0; iParticle<nParticles; iParticle++){

    int particleSpecies = particles.at(iParticle);


    if (outFile){
      outFile->cd();
    }

    if (draw){
      spectraCanvas->cd();
      spectraFrame = spectraCanvas->DrawFrame(0,yMin,MAX_MTM0+.1,yMax);
      spectraCanvas->Update();
      
      ratioCanvas->cd();
      ratioFrame = ratioCanvas->DrawFrame(0,0,MAX_MTM0+.1,1.2);
      ratioCanvas->Update();
    }
    

    for (unsigned int iCentBin=0; iCentBin<nCentralityBins; iCentBin++){

      //If the user has requested a (reasonable) specific bin then skip others
      if (userCentIndex >= 0 && userCentIndex < (int)nCentralityBins && userCentIndex != (int)iCentBin)
	continue;

      //--- Get the Number of Events in this Centrality Bin ---
      Double_t nEvents  = (double)nEventsHisto->GetBinContent(nEventsHisto->FindBin(iCentBin));
      
      //Get the 3D Yield Histos
      zTPCPlusHisto3D = (TH3D *)yieldHistoFile->
	Get(Form("YieldHistograms/zTPC_%s_Cent%d_Total",
		 particleInfo->GetParticleName(particleSpecies,1).Data(),
		 iCentBin));
      zTPCMinusHisto3D = (TH3D *)yieldHistoFile->
	Get(Form("YieldHistograms/zTPC_%s_Cent%d_Total",
		 particleInfo->GetParticleName(particleSpecies,-1).Data(),
		 iCentBin));
      zTOFPlusHisto3D = (TH3D *)yieldHistoFile->
	Get(Form("YieldHistograms/zTOF_%s_Cent%d_Total",
		 particleInfo->GetParticleName(particleSpecies,1).Data(),
		 iCentBin));
      zTOFMinusHisto3D = (TH3D *)yieldHistoFile->
	Get(Form("YieldHistograms/zTOF_%s_Cent%d_Total",
		 particleInfo->GetParticleName(particleSpecies,-1).Data(),
		 iCentBin));
      
      //Create the Spectra
      if (outFile)
	outFile->cd();      
      CreateSpectra(spectraPlus,particleSpecies,1,iCentBin,0);
      CreateSpectra(spectraMinus,particleSpecies,-1,iCentBin,0);
      CreateSpectra(spectraPlusTOF,particleSpecies,1,iCentBin,1);
      CreateSpectra(spectraMinusTOF,particleSpecies,-1,iCentBin,1);
      CreateSpectra(spectraRatio,particleSpecies,0,iCentBin,-1);
	
      for (int yIndex=GetMinRapidityIndexOfInterest(particleSpecies);
	   yIndex<=GetMaxRapidityIndexOfInterest(particleSpecies); yIndex++){
	
	//Skip rapidity bins that were not requested
	if (userRapidityIndex >=0 && (int)yIndex != userRapidityIndex)
	  continue;

	//Skip if there are not pid curves for this bin
	if (!particleInfo->SetEmpiricalBichselFunction(particleSpecies,yIndex))
	  continue;

	STOP_TPC_SPECTRUM = false;
	STOP_TOF_SPECTRUM = false;
	
	double previousElectronAmp(-1);
	std::vector<std::pair<double,bool> > prevTofParams;
	/**************************************************************************/
	/****************** YIELD EXTRACTION *************************************/
	const int minEntries(100);
	for (unsigned int mTm0Index=0; mTm0Index<nmTm0Bins; mTm0Index++){

	  double mTm0 = GetmTm0RangeCenter(mTm0Index);
	  if (mTm0 < MIN_MTM0 || mTm0 > MAX_MTM0)
	    continue;

	  //Kinematic Info
	  double mass = particleInfo->GetParticleMass(particleSpecies);
	  double rapidity = GetRapidityRangeCenter(yIndex);
	  double mT = mTm0 + mass;
	  double pT = ConvertmTm0ToPt(mTm0,mass);
	  double pZ = ComputepZ(mT,rapidity);
	  double pTotal = ComputepTotal(pT,pZ);

	  //Make the Total Yield 1D Histos
	  TH1D *zTPCPlusHisto  = GetYieldHistogram(zTPCPlusHisto3D,rapidity,mTm0,"TPC");
	  TH1D *zTPCMinusHisto = GetYieldHistogram(zTPCMinusHisto3D,rapidity,mTm0,"TPC");
	  TH1D *zTOFPlusHisto  = GetYieldHistogram(zTOFPlusHisto3D,rapidity,mTm0,"TOF");
	  TH1D *zTOFMinusHisto = GetYieldHistogram(zTOFMinusHisto3D,rapidity,mTm0,"TOF");	  
	  
	  //Set Marker Styles of the Yield Histograms
	  zTPCPlusHisto->SetMarkerStyle(kOpenCircle);
	  zTPCMinusHisto->SetMarkerStyle(kOpenCircle);
	  zTOFPlusHisto->SetMarkerStyle(kOpenCircle);
	  zTOFMinusHisto->SetMarkerStyle(kOpenCircle);
	  zTPCPlusHisto->SetMarkerSize(1.2);
	  zTPCMinusHisto->SetMarkerSize(1.2);
	  zTOFPlusHisto->SetMarkerSize(1.2);
	  zTOFMinusHisto->SetMarkerSize(1.2);

	  //----------- TPC YIELD EXTRACTION -------------------------
	  
	  //Get the TOF Optimized Fits for this bin
	  std::vector <TF1 *> zTOFOptzTPCFits;
	  bool goodTofOptFits = true;
	  for (int i=0; i<=DEUTERON; i++){
	    
	    if (i == ELECTRON){
	      zTOFOptzTPCFits.push_back(new TF1("electronFit","gaus(0)",-2,2));
	      zTOFOptzTPCFits.back()->FixParameter(0,10);
	      zTOFOptzTPCFits.back()->FixParameter(1,particleInfo->PredictZTPC(pTotal,particleSpecies,ELECTRON));
	      zTOFOptzTPCFits.back()->FixParameter(2,zTOFOptzTPCFits.at(PION) ? zTOFOptzTPCFits.at(PION)->GetParameter(2) : .08);
	      continue;
	    }
	    
	    zTOFOptzTPCFits.push_back((TF1 *)pidFile->
				      Get(Form("TOFOptimizedFits_%s/tofOptFit_%s_%s_yIndex%02d_mTm0Index%02d",
					       particleInfo->GetParticleName(particleSpecies).Data(),
					       particleInfo->GetParticleName(particleSpecies).Data(),
					       particleInfo->GetParticleName(i).Data(),
					       yIndex,mTm0Index)));
	  }

	  //Make sure that at at least the tof Optimized Fit for the particle of interest exists	  
	  if (!zTOFOptzTPCFits.at(particleSpecies)){
	    cout <<"INFO - GenerateRawSpectrum() - Tof Optimized fit is not available for this particle. SKIPPING BIN!\n";
	    goodTofOptFits = false;
	  }
	  
	  //Compute Separation between the peaks
	  double minSeparation(9999);
	  if (goodTofOptFits){
	    for (unsigned int i=0; i<zTOFOptzTPCFits.size(); i++){
	      if (particleSpecies == (int)i)
		continue;
	      if (!zTOFOptzTPCFits.at(i))
		continue;
	      minSeparation = TMath::Min(minSeparation,fabs(zTOFOptzTPCFits.at(particleSpecies)->GetParameter(1) -
							    zTOFOptzTPCFits.at(i)->GetParameter(1)));
	    }
	  }

	  //If the particle of interst is the kaon, skip bins in which the electron is
	  //under the kaon peak
	  bool kaonConditional = true;
	  if (particleSpecies == KAON &&
	      fabs(particleInfo->PredictZTPC(pTotal,particleSpecies,ELECTRON)) < 1.0 * .08)
	    kaonConditional = false;
	  
	  
	  //Only do the TPC Extraction if the peaks are separated by more than 2 sigma
	  if (minSeparation < 1.5*.08 && mTm0 > .4){
	    cout <<"Stopping TPC Extraction due to min separation: " <<minSeparation <<" " <<mTm0 <<endl;
	    STOP_TPC_SPECTRUM = true;
	  }
	  if (!STOP_TPC_SPECTRUM && goodTofOptFits &&
	      zTPCPlusHisto->GetEntries() > minEntries &&
	      zTPCMinusHisto->GetEntries() > minEntries && kaonConditional){
	    
	    YieldExtractionFit fitzTPCPlus(particleSpecies, yIndex, mTm0Index,
					   zTPCPlusHisto,particleInfo, zTOFOptzTPCFits);
	    YieldExtractionFit fitzTPCMinus(particleSpecies, yIndex, mTm0Index, 
					    zTPCMinusHisto,particleInfo, zTOFOptzTPCFits);

	    if (previousElectronAmp > 0 && mTm0 > .2){
	      fitzTPCPlus.SetElectronAmpMax(previousElectronAmp);
	    }
	    
	    if (particleSpecies == PROTON){
	      fitzTPCPlus.FixAmpParameter(ELECTRON,0);
	    }
	    
	    //------- Plus
	    if (zTPCPlusHisto->GetEntries() > minEntries){
	      zTPCPlusHisto->GetXaxis()->
		SetRangeUser(fitzTPCPlus.GetMinFitRange(),fitzTPCPlus.GetMaxFitRange());
	      
	      int status = fitzTPCPlus.Fit();
	      if (status > 0){
		std::pair<double,double> yield = fitzTPCPlus.ComputeYield(nEvents);	      
		spectraPlus.at(yIndex)->SetPoint(spectraPlus.at(yIndex)->GetN(),fitzTPCPlus.GetmTm0(),yield.first);
		spectraPlus.at(yIndex)->SetPointError(spectraPlus.at(yIndex)->GetN()-1,mTm0BinWidth/2.0,yield.second);

		outFile->cd();
		outFile->cd(Form("YieldExtractionFits_%s/Cent%02d",
				 particleInfo->GetParticleName(particles.at(iParticle),1).Data(),
				 iCentBin));
		fitzTPCPlus.GetFit()->Write(fitzTPCPlus.GetFit()->GetName(),TObject::kOverwrite);
		for (unsigned int iFit=0; iFit<fitzTPCPlus.GetIndividualFits()->size(); iFit++){
		  fitzTPCPlus.GetIndividualFits()->at(iFit)->Write("",TObject::kOverwrite);
		}
		//zTPCPlusHisto->Write(zTPCPlusHisto->GetName(),
		//					      TObject::kOverwrite);

		outFile->cd();
		outFile->cd(Form("YieldExtractionGraphs_%s/Cent%02d",
				 particleInfo->GetParticleName(particles.at(iParticle),1).Data(),
				 iCentBin));
		fitzTPCPlus.GetYieldGraph()->Write(fitzTPCPlus.GetYieldGraph()->GetName(),
						   TObject::kOverwrite);
		fitzTPCPlus.GetNotYieldGraph()->Write(fitzTPCPlus.GetNotYieldGraph()->GetName(),
						      TObject::kOverwrite);
	      }
	    }
	    
	    //------- Minus
	    if (zTPCMinusHisto->GetEntries() > minEntries){

	      if (particleSpecies == PROTON)
		fitzTPCMinus.FixAmpParameter(ELECTRON,fitzTPCPlus.GetAmpParameter(ELECTRON));
	      else
		fitzTPCMinus.SetElectronAmpMax(fitzTPCPlus.GetAmpParameter(ELECTRON));

	      
	      fitzTPCMinus.FixHorizontalSlushParameter(fitzTPCPlus.GetSlushParameter());
	      zTPCMinusHisto->GetXaxis()->
		SetRangeUser(fitzTPCMinus.GetMinFitRange(),fitzTPCMinus.GetMaxFitRange());
	      
	      int status = fitzTPCMinus.Fit();
	      if (status > 0){
		std::pair<double,double> yield =
		  fitzTPCMinus.ComputeYield(nEvents,
					    fitzTPCPlus.GetFitConfidenceInterval(),
					    fitzTPCPlus.GetFit());	      
		spectraMinus.at(yIndex)->SetPoint(spectraMinus.at(yIndex)->GetN(),fitzTPCMinus.GetmTm0(),yield.first);
		spectraMinus.at(yIndex)->SetPointError(spectraMinus.at(yIndex)->GetN()-1,mTm0BinWidth/2.0,yield.second);

		outFile->cd();
		outFile->cd(Form("YieldExtractionFits_%s/Cent%02d",
				 particleInfo->GetParticleName(particles.at(iParticle),-1).Data(),
				 iCentBin));
		fitzTPCMinus.GetFit()->Write(fitzTPCMinus.GetFit()->GetName(),TObject::kOverwrite);
		for (unsigned int iFit=0; iFit<fitzTPCMinus.GetIndividualFits()->size(); iFit++){
		  fitzTPCMinus.GetIndividualFits()->at(iFit)->Write("",TObject::kOverwrite);
		}
		//zTPCMinusHisto->Write(zTPCMinusHisto->GetName(),
		//					       TObject::kOverwrite);
		
		outFile->cd();
		outFile->cd(Form("YieldExtractionGraphs_%s/Cent%02d",
				 particleInfo->GetParticleName(particles.at(iParticle),-1).Data(),
				 iCentBin));
		fitzTPCMinus.GetYieldGraph()->Write(fitzTPCMinus.GetYieldGraph()->GetName(),
						    TObject::kOverwrite);
		fitzTPCMinus.GetNotYieldGraph()->Write(fitzTPCMinus.GetNotYieldGraph()->GetName(),
						       TObject::kOverwrite);
	      }
	    }
	    
	    previousElectronAmp = fitzTPCPlus.GetAmpParameter(ELECTRON);
	    
	    if (draw){
	      yieldCanvas->cd();
	      yieldCanvas->cd(1);
	      zTPCPlusHisto->Draw("E");
	      fitzTPCPlus.DrawFit("SAME");
	      fitzTPCPlus.DrawIndividualDistributions();
	      gPad->Update();
	      
	      yieldCanvas->cd(2);
	      zTPCMinusHisto->Draw("E");
	      fitzTPCMinus.DrawFit("SAME");
	      fitzTPCMinus.DrawIndividualDistributions();
	      gPad->Update();

	      yieldCanvas->Update();
	      
	      spectraCanvas->cd();
	      spectraPlus.at(yIndex)->Draw("PZ");
	      spectraMinus.at(yIndex)->Draw("PZ");
	      spectraCanvas->Update();

	      gSystem->Sleep(300);

	    }//End Draw

	  }//End TPC YIELD EXTRACTION

	  //----------- TOF YIELD EXTRACTION -------------------------
	  std::vector <TF1 *> zTOFFits;
	  bool goodTofFits = true;
	  for (int i=0; i<=PROTON; i++){
	    zTOFFits.push_back((TF1 *)pidFile->Get(Form("TOFFits_%s/tofFit_%s_%s_yIndex%02d_mTm0Index%02d",
							particleInfo->GetParticleName(particleSpecies).Data(),
							particleInfo->GetParticleName(particleSpecies).Data(),
							particleInfo->GetParticleName(i).Data(),
							yIndex,mTm0Index)));
	  }

	  //Make sure that at least the TOF Fit for the particle of interest exists
	  if (!zTOFFits.at(particleSpecies)){
	    cout <<"INFO - GenerateRawSpectra() - TOF Fit not available for this particle. Skipping!\n";
	    goodTofFits = false;
	  }

	  //If we are over 800MeV in mTm0 then make sure there are three tof Fits
	  if (mTm0 > .8){
	    int nTofFits(0);
	    for (unsigned int i=0; i<zTOFFits.size(); i++){
	      if (!zTOFFits.at(i))
		continue;
	      nTofFits++;
	    }
	    if (nTofFits != 3){
	      goodTofFits = false;
	      break;
	    }
	  }
	  
	  if (!STOP_TOF_SPECTRUM && goodTofFits){
	    YieldExtractionFit fitzTOFPlus(particleSpecies, yIndex, mTm0Index, 
					   zTOFPlusHisto,particleInfo,zTOFFits,true);
	    YieldExtractionFit fitzTOFMinus(particleSpecies, yIndex, mTm0Index, 
					    zTOFMinusHisto,particleInfo,zTOFFits,true);

	    
	    //-------- Plus
	    bool isGoodPlusFit = false;
	    if (goodTofFits && zTOFPlusHisto->GetEntries() > minEntries){
	      zTOFPlusHisto->GetXaxis()->SetRangeUser(fitzTOFPlus.GetMinFitRange()-.2,
						      fitzTOFPlus.GetMaxFitRange()+.2);

	      int status = fitzTOFPlus.Fit();
	      if (status > 0){
		std::pair<double,double> yield = fitzTOFPlus.ComputeYield(nEvents);	      
		spectraPlusTOF.at(yIndex)->SetPoint(spectraPlusTOF.at(yIndex)->GetN(),fitzTOFMinus.GetmTm0(),yield.first);
		spectraPlusTOF.at(yIndex)->SetPointError(spectraPlusTOF.at(yIndex)->GetN()-1,mTm0BinWidth/2.0,yield.second);

		outFile->cd();
		outFile->cd(Form("YieldExtractionFits_%s/Cent%02d",
				 particleInfo->GetParticleName(particles.at(iParticle),1).Data(),
				 iCentBin));
		fitzTOFPlus.GetFit()->Write(fitzTOFPlus.GetFit()->GetName(),TObject::kOverwrite);
		for (unsigned int iFit=0; iFit<fitzTOFPlus.GetIndividualFits()->size(); iFit++){
		  fitzTOFPlus.GetIndividualFits()->at(iFit)->Write("",TObject::kOverwrite);
		}

		outFile->cd();
		outFile->cd(Form("YieldExtractionGraphs_%s/Cent%02d",
				 particleInfo->GetParticleName(particles.at(iParticle),1).Data(),
				 iCentBin));
		fitzTOFPlus.GetYieldGraph()->Write(fitzTOFPlus.GetYieldGraph()->GetName(),
						   TObject::kOverwrite);
		fitzTOFPlus.GetNotYieldGraph()->Write(fitzTOFPlus.GetNotYieldGraph()->GetName(),
						   TObject::kOverwrite);

		isGoodPlusFit = true;
	      }
	    }
	    
	    
	    //-------- Minus
	    if (goodTofFits && zTOFMinusHisto->GetEntries() > minEntries && isGoodPlusFit){
	      zTOFMinusHisto->GetXaxis()->SetRangeUser(fitzTOFMinus.GetMinFitRange()-.2,
										fitzTOFMinus.GetMaxFitRange()+.2);

	      int status = fitzTOFMinus.Fit();
	      if (status > 0){
		std::pair<double,double> yield =
		  fitzTOFMinus.ComputeYield(nEvents,
					    fitzTOFPlus.GetFitConfidenceInterval(),
					    fitzTOFPlus.GetFit());	      
		spectraMinusTOF.at(yIndex)->SetPoint(spectraMinusTOF.at(yIndex)->GetN(),fitzTOFMinus.GetmTm0(),yield.first);
		spectraMinusTOF.at(yIndex)->SetPointError(spectraMinusTOF.at(yIndex)->GetN()-1,mTm0BinWidth/2.0,yield.second);

		outFile->cd();
		outFile->cd(Form("YieldExtractionFits_%s/Cent%02d",
				 particleInfo->GetParticleName(particles.at(iParticle),-1).Data(),
				 iCentBin));
		fitzTOFMinus.GetFit()->Write(fitzTOFMinus.GetFit()->GetName(),TObject::kOverwrite);
		for (unsigned int iFit=0; iFit<fitzTOFMinus.GetIndividualFits()->size(); iFit++){
		  fitzTOFMinus.GetIndividualFits()->at(iFit)->Write("",TObject::kOverwrite);
		}

		outFile->cd();
		outFile->cd(Form("YieldExtractionGraphs_%s/Cent%02d",
				 particleInfo->GetParticleName(particles.at(iParticle),-1).Data(),
				 iCentBin));
		fitzTOFMinus.GetYieldGraph()->Write(fitzTOFMinus.GetYieldGraph()->GetName(),
						    TObject::kOverwrite);
		fitzTOFMinus.GetNotYieldGraph()->Write(fitzTOFMinus.GetNotYieldGraph()->GetName(),
						       TObject::kOverwrite);
	      }
	    }

	    double minTofSeparation(9999);
	    double particleOfInterestTofMean = fitzTOFPlus.GetFit()->GetParameter(particleSpecies*3+1);
	    double particleOfInterestTofWidth = fitzTOFPlus.GetFit()->GetParameter(particleSpecies*3+2);
	    for (int i=0; i<(int)fitzTOFPlus.GetFit()->GetNpar()/3; i++){
	      if (particleSpecies == i)
		continue;
	      minTofSeparation = TMath::Min(minTofSeparation,
					    fabs(particleOfInterestTofMean -
						 fitzTOFPlus.GetFit()->GetParameter(i*3+1)));
	    }
	    if (minTofSeparation < 4.0*particleOfInterestTofWidth){
	      STOP_TOF_SPECTRUM = true;
	      cout <<"Stopping TOF spectrum - Minimum Separation distance condition not met." <<endl;
	    }
	    
	    if (draw){
	      yieldCanvas->cd(3);
	      zTOFPlusHisto->Draw("E");
	      if (fitzTOFPlus.GetFitConfidenceInterval())
		fitzTOFPlus.GetFitConfidenceInterval()->Draw("2");
	      fitzTOFPlus.DrawFit("SAME");
	      fitzTOFPlus.DrawIndividualDistributions();
	      gPad->Update();
	      
	      yieldCanvas->cd(4);
	      zTOFMinusHisto->Draw("E");
	      if (fitzTOFMinus.GetFitConfidenceInterval())
		fitzTOFMinus.GetFitConfidenceInterval()->Draw("2");
	      fitzTOFMinus.DrawFit("SAME");
	      fitzTOFMinus.DrawIndividualDistributions();
	      gPad->Update();
	      
	      yieldCanvas->Update();
	      
	      spectraCanvas->cd();
	      spectraPlusTOF.at(yIndex)->Draw("PZ");
	      spectraMinusTOF.at(yIndex)->Draw("PZ");
	      spectraCanvas->Update();
	      
	      gSystem->Sleep(1000);
	      
	    }
	  }//End TOF EXTRACTION

	  if (zTPCPlusHisto)
	    delete zTPCPlusHisto;
	  if (zTPCMinusHisto)
	    delete zTPCMinusHisto;
	  if (zTOFPlusHisto)
	    delete zTOFPlusHisto;
	  if (zTOFMinusHisto)
	    delete zTOFMinusHisto;

	  for (unsigned int i=0; i<zTOFOptzTPCFits.size(); i++){
	    if (zTOFOptzTPCFits.at(i))
	      delete zTOFOptzTPCFits.at(i);
	    zTOFOptzTPCFits.clear();
	  }

	  
	  
	}//End Loop Over mTm0 Bins
	/**************************************************************************/

	
	//Check that the tpc spectra exist
	if (!spectraPlus.at(yIndex))
	  continue;
	if (spectraPlus.at(yIndex)->GetN() < 3)
	  continue;

	
	//Overall TOF Matching Efficiency Correction
	cout <<"Performing overall TOF Matching Efficiency Correction\n";
	if (spectraPlusTOF.at(yIndex))
	  ScaleTofSpectrum(spectraPlus.at(yIndex),spectraPlusTOF.at(yIndex));
	if (spectraMinusTOF.at(yIndex))
	  ScaleTofSpectrum(spectraMinus.at(yIndex),spectraMinusTOF.at(yIndex));
	
	
	//Check the stitch value
	Double_t maxTPC = spectraPlus.at(yIndex)->GetX()[TMath::Max(0,spectraPlus.at(yIndex)->GetN()-1)];
	
	Double_t stitchVal = TPC_TOF_STITCH;
	if (maxTPC < TPC_TOF_STITCH)
	  stitchVal = maxTPC;

	if (particleSpecies == KAON){

	  double val = spectraPlus.at(yIndex)->GetX()[0];
	  for (int iPoint=0; iPoint<spectraPlus.at(yIndex)->GetN(); iPoint++){

	    if (fabs(val-spectraPlus.at(yIndex)->GetX()[iPoint]) > mTm0BinWidth/2.0){
	      stitchVal = val + mTm0BinWidth/2.0;
	      break;
	    }
	    
	    val = spectraPlus.at(yIndex)->GetX()[iPoint];
	  }
	}

	/*
	Double_t stitchVal = TPC_TOF_STITCH;
	if (spectraPlusTOF.at(yIndex)->GetN() > 5){

	  if (particleSpecies == PION){
	    if (maxTPC < TPC_TOF_STITCH)
	      stitchVal = maxTPC;
	    else
	      stitchVal = TPC_TOF_STITCH;
	  }
	  else if (particleSpecies == KAON){
	    stitchVal = spectraPlusTOF.at(yIndex)->GetX()[5];
	  }
	  else if (particleSpecies == PROTON){
	    if (maxTPC < TPC_TOF_STITCH)
	      stitchVal = maxTPC;
	    else 
	      stitchVal = TPC_TOF_STITCH;
	  }
	}
	*/
	  
	//Remove Points from the TPC Spectrum that are above the stitch value
	cout <<"Removing Points from the TPC spectrum that are above the stitch value\n";
	if (spectraPlus.at(yIndex))
	  RemovePointsAbove(spectraPlus.at(yIndex),stitchVal);
	if (spectraMinus.at(yIndex))
	  RemovePointsAbove(spectraMinus.at(yIndex),stitchVal);

	//Remove Points from the TOF Spectrum that are below the stitch value
	cout <<"Removing points from the TOF Spectrum that are below the stitch value\n";
	if (spectraPlusTOF.at(yIndex))
	  RemovePointsBelow(spectraPlusTOF.at(yIndex),stitchVal);
	if (spectraMinusTOF.at(yIndex))
	  RemovePointsBelow(spectraMinusTOF.at(yIndex),stitchVal);
	
	if (draw){
	  spectraCanvas->cd();
	  spectraPlus.at(yIndex)->Draw("PZ");
	  spectraPlusTOF.at(yIndex)->Draw("PZ");
	  gPad->Update();
	  spectraMinus.at(yIndex)->Draw("PZ");
	  spectraMinusTOF.at(yIndex)->Draw("PZ");
	}
	
	//Construct the Ratio of the Spectra
	ConstructSpectraRatio(spectraPlus.at(yIndex),spectraMinus.at(yIndex),spectraRatio.at(yIndex));
	ConstructSpectraRatio(spectraPlusTOF.at(yIndex),spectraMinusTOF.at(yIndex),spectraRatio.at(yIndex));

	if (draw){
	  ratioCanvas->cd();
	  spectraRatio.at(yIndex)->Draw("PZ");
	  gPad->Update();
	  
	  gSystem->Sleep(5000);
	}
	
	//Save Spectra
	if(outFile){
	  outFile->cd();
	  outFile->cd(Form("RawSpectra_%s",particleInfo->GetParticleName(particleSpecies,1).Data()));

	  if (spectraPlus.at(yIndex)->GetN() > 0)
	    spectraPlus.at(yIndex)->Write(spectraPlus.at(yIndex)->GetName(),TObject::kOverwrite);
	  if (spectraPlus.at(yIndex)->GetN() > 0)
	    spectraPlusTOF.at(yIndex)->Write(spectraPlusTOF.at(yIndex)->GetName(),TObject::kOverwrite);
	  
	  
	  outFile->cd();
	  gDirectory->cd(Form("RawSpectra_%s",particleInfo->GetParticleName(particleSpecies,-1).Data()));
	  
	  if (spectraMinus.at(yIndex)->GetN() > 0)
	    spectraMinus.at(yIndex)->Write(spectraMinus.at(yIndex)->GetName(),TObject::kOverwrite);
	  if (spectraMinusTOF.at(yIndex)->GetN() > 0)
	    spectraMinusTOF.at(yIndex)->Write(spectraMinusTOF.at(yIndex)->GetName(),TObject::kOverwrite);

	  outFile->cd();
	  gDirectory->cd(Form("SpectraRatio_%s",particleInfo->GetParticleName(particles.at(iParticle)).Data()));
	  if (spectraRatio.at(yIndex)->GetN() > 0)
	    spectraRatio.at(yIndex)->Write(spectraRatio.at(yIndex)->GetName(),TObject::kOverwrite);

	  delete spectraPlus.at(yIndex);
	  delete spectraPlusTOF.at(yIndex);
	  delete spectraMinus.at(yIndex);
	  delete spectraMinusTOF.at(yIndex);
	  delete spectraRatio.at(yIndex);
	}

      }//End Loop Over Rapidity Bins

      //Clean Up
      if (zTPCPlusHisto3D)
	delete zTPCPlusHisto3D;
      if (zTPCMinusHisto3D)
	delete zTPCMinusHisto3D;
      if (zTOFPlusHisto3D)
	delete zTOFPlusHisto3D;
      if (zTOFMinusHisto3D)
	delete zTOFMinusHisto3D;
      
    }//End Loop Over Centrality Bins
  }//End Loop Over Particles

  if (outFile)
    outFile->Close();
  

}



/*
//__________________________________________________________________
Double_t ComputeSystematicErrors(CurrentBinInfo *binInfo, Double_t nEvents, Int_t plusOrMinus,
				 std::vector<std::pair<double,double> > particleParamErrs){

  //Vary all the parameters of the fit to the values passed in in particleParamErrs.

  TH1D *yieldHisto;
  TF1 *fitFunc;
  Double_t nominalYield;
  Int_t speciesInterest = binInfo->GetParticleOfInterest();
  Double_t mTm0 = binInfo->GetmTm0();

  if (plusOrMinus > 0){
    yieldHisto = binInfo->GetZPlusHisto();
    fitFunc    = binInfo->GetZPlusFit();
    nominalYield = binInfo->GetYieldPlus();
  }
  else if (plusOrMinus < 0){
    yieldHisto = binInfo->GetZMinusHisto();
    fitFunc    = binInfo->GetZMinusFit();
    nominalYield = binInfo->GetYieldMinus();
  }
  else {
    cout <<"ERROR - ExtractYields.cxx :: ComputeSystematicErrors() plusOrMinus can only be +1(plus) or -1(minus). EXITING!";
    exit (EXIT_FAILURE);
  }
  
  std::vector<double> yields(0);
  const int nPars = fitFunc->GetNpar();

  //We need to loop Over all the parameters twice - vary them high and low
  for (int iHighLow=0; iHighLow<2; iHighLow++){
    
    Double_t highOrLow(1);
    if (iHighLow == 0)
      highOrLow = 1.0;
    else if (iHighLow == 1)
      highOrLow = -1.0;
    
    for (int iPar=0; iPar<nPars; iPar++){
      
      //if (iPar/3.0 == speciesInterest)
      //continue;
      
      //Copy the fit function into a new function, fix all the parameters
      TF1 tempFunc(*fitFunc);
      tempFunc.SetLineColor(kRed);
      
      for (int jPar=0; jPar<tempFunc.GetNpar(); jPar++){
	
	if (jPar/3 == speciesInterest){
	  tempFunc.SetParameter(jPar,fitFunc->GetParameter(jPar));
	}
	else {
	  //Amplitudes
	  if (jPar%3==0){
	    tempFunc.SetParameter(jPar,TMath::Max(0.0,fitFunc->GetParameter(jPar)+fitFunc->GetParError(jPar)*highOrLow));
	    tempFunc.SetParLimits(jPar,0,tempFunc.GetParameter(jPar)*1.1);
	  }
	  //Means
	  else if (jPar%3 == 1){
	    tempFunc.FixParameter(jPar,fitFunc->GetParameter(jPar)+particleParamErrs.at(jPar/3).first*highOrLow);
	  }
	  //Widths
	  else if (jPar%3 == 2){
	    tempFunc.FixParameter(jPar,fitFunc->GetParameter(jPar)+particleParamErrs.at(jPar/3).second*highOrLow);
	  }
	}
	
      }//End Loop Over jPar

      yieldHisto->Fit(&tempFunc,"RNQL");
      
      //Compute the Yield
      std::pair<double,double> yield = ComputeYield(yieldHisto,&tempFunc,speciesInterest,binInfo->GetmT(),nEvents);
      yields.push_back(yield.first);

      
    }//End Loop Over All Parameters iPars
    
  }//End Loop Over High and Low
  
  return TMath::RMS(yields.size(),&yields.at(0));
  
}
*/

//_________________________________________________________________________________
void CreateSpectra(std::vector<TGraphErrors *> &spectraVec, int pid, int charge, unsigned int iCentBin, int detector){

  if (spectraVec.size() > 0)
    spectraVec.clear();

  if (spectraVec.size() == 0)
    spectraVec.resize(nRapidityBins,(TGraphErrors *)NULL);

  ParticleInfo particleInfo;
  const unsigned int nCentralityBins = GetNCentralityBins();

  TString mod="";
  if (detector == 1)
    mod = "TOF";
  if (detector < 0)
    mod = "Ratio";

  TString mod1="Uncorrected TPC Spectrum";
  if (detector == 1)
    mod1 = "Uncorrected TOF Spectrum";
  else if (detector < 0)
    mod1 = "Ratio";
  
  //Loop Over the rapidity bins and create all of the spectra Graphs
  for (unsigned int yIndex=0; yIndex<nRapidityBins; yIndex++){

    spectraVec.at(yIndex) = new TGraphErrors();

    
    spectraVec.at(yIndex)->SetName(Form("rawSpectra%s_%s_Cent%02d_yIndex%02d",
					mod.Data(),
					particleInfo.GetParticleName(pid,charge).Data(),
					iCentBin,yIndex));
    spectraVec.at(yIndex)->SetTitle(Form("%s %s Cent=[%d,%d]%% y_{%s}=[%.02f,%.02f];m_{T}-m_{%s} (GeV/c^{2});%s",
					 mod1.Data(),
					 particleInfo.GetParticleSymbol(pid,charge).Data(),
					 iCentBin!=nCentralityBins-1 ? (int)GetCentralityPercents().at(iCentBin+1):0,
					 (int)GetCentralityPercents().at(iCentBin),
					 particleInfo.GetParticleSymbol(pid).Data(),
					 GetRapidityRangeLow(yIndex),GetRapidityRangeHigh(yIndex),
					 particleInfo.GetParticleSymbol(pid).Data(),
					 GetSpectraAxisTitle().Data()));
    
    spectraVec.at(yIndex)->SetMarkerStyle(particleInfo.GetParticleMarker(pid,charge));
    spectraVec.at(yIndex)->SetMarkerColor(particleInfo.GetParticleColor(pid));
    if (pid == KAON)
      spectraVec.at(yIndex)->SetMarkerSize(1.5);

  }//End Loop Over rapidity bins

}

//____________________________________________________________
void ScaleTofSpectrum(TGraphErrors *tpcSpectrum, TGraphErrors *tofSpectrum){

  //The TOF Matching efficiency correction applied during the skimmer binner corrects
  //for how the matching efficiency changes as a function of mT-m0, but there is often
  //an overall correction that needs to be applied which is momentum independent.

  if (tofSpectrum->GetN() < 1)
    return;
  
  std::vector<double> tofToTpcRatio;
  for (int iPoint=0; iPoint<tpcSpectrum->GetN(); iPoint++){
    
    int tofSpectrumPoint = TMath::BinarySearch(tofSpectrum->GetN(),
					       tofSpectrum->GetX(),
					       tpcSpectrum->GetX()[iPoint]);
    if (fabs(tofSpectrum->GetX()[tofSpectrumPoint] -
	     tpcSpectrum->GetX()[iPoint]) > mTm0BinWidth)
      continue;

    //Only consider points with mT-m0 above .4 GeV and less than the stitch
    //value - where the TOF is considered reliable
    if (tofSpectrum->GetX()[tofSpectrumPoint] < 0.3 || tofSpectrum->GetX()[tofSpectrumPoint] > TPC_TOF_STITCH)
      continue;
    
    tofToTpcRatio.push_back(tofSpectrum->GetY()[tofSpectrumPoint] /
			    tpcSpectrum->GetY()[iPoint]);
    
  }

  Double_t correctionFactor(1);
  
  //If there are no overlapping points between the TPC and TOF Spectra  
  if (tofToTpcRatio.size() == 0){
    cout <<"WARNING - generateRawSpectra::ScaleTofSpectrum() - No overlap between TPC and TOF Spectra!\n"
	 <<"          Can not apply momentum independent scaling to to TOF Spectrum!\n";

    //If there are no overlapping points to determine what the correction factor
    //should be for the tof spectrum, then fit an exponential to the last three points
    //of the tpc spectrum, extrapolated the fit to the first value of the tof spectrum,
    //take the ratio of the function to the tof yield, and use that as the correction factor.

    Double_t min = tpcSpectrum->GetX()[tpcSpectrum->GetN()-3] - mTm0BinWidth/2.0;
    
    TF1 expoExtrapolation("","expo",min,2);
    tpcSpectrum->Fit(&expoExtrapolation,"RNQ");

    Double_t firstTofVal = tofSpectrum->GetY()[0];

    correctionFactor = firstTofVal / expoExtrapolation.Eval(tofSpectrum->GetX()[0]);
    
    //return;
  }
  //If ther are overlapping points then simply use the average ratio
  else {
    correctionFactor = TMath::Mean(tofToTpcRatio.size(),&tofToTpcRatio.at(0));
  }

  //Apply the Correction
  TGraphScale(tofSpectrum,1.0/correctionFactor,false);

}

//____________________________________________________________
void RemovePointsAbove(TGraphErrors *graph, Double_t xVal){

  if (graph->GetN() < 1)
    return;
  
  //Remove Points in the graph above xVal
  for (int iPoint=graph->GetN(); iPoint>=0; iPoint--){

    if (graph->GetX()[iPoint] > xVal)
      graph->RemovePoint(iPoint);
  }
  

}

//____________________________________________________________
void RemovePointsBelow(TGraphErrors *graph, Double_t xVal){

  if (graph->GetN() < 1)
    return;
  
  //Remove Points in the graph below xVal
  for (int iPoint=graph->GetN(); iPoint>=0; iPoint--){

    if (graph->GetX()[iPoint] <= xVal)
      graph->RemovePoint(iPoint);
  }
  

}

//___________________________________________________________
void ConstructSpectraRatio(TGraphErrors *spectraPlus, TGraphErrors *spectraMinus,
			   TGraphErrors *spectraRatio){

  const int nPoints = TMath::Min(spectraPlus->GetN(),spectraMinus->GetN());
  for (int iPoint=0; iPoint<nPoints; iPoint++){

    int iPointMinus = TMath::BinarySearch(spectraMinus->GetN(),
					  spectraMinus->GetX(),
					  spectraPlus->GetX()[iPoint]);
    if (fabs(spectraPlus->GetX()[iPoint]-spectraMinus->GetX()[iPointMinus]) > mTm0BinWidth/2.0)
      continue;
    
    
    Double_t yieldPlus = spectraPlus->GetY()[iPoint];    
    Double_t yieldMinus = spectraMinus->GetY()[iPointMinus];
    Double_t yieldPlusErr = spectraPlus->GetEY()[iPoint];
    Double_t yieldMinusErr = spectraMinus->GetEY()[iPointMinus];
    
    Double_t ratio = yieldPlus/yieldMinus;
    Double_t ratioErr = ratio * sqrt(pow((yieldPlusErr/yieldPlus),2) +
				     pow((yieldMinusErr/yieldMinus),2));

    Double_t meanX = (spectraPlus->GetX()[iPoint]+spectraMinus->GetX()[iPoint])/2.0;
    
    spectraRatio->SetPoint(spectraRatio->GetN(),meanX,ratio);
    spectraRatio->SetPointError(spectraRatio->GetN()-1,0,ratioErr);
  }
  
}



