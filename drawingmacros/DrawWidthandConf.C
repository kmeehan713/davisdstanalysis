#include "../inc/globalDefinitions.h"

/* Plots the ratio of the Minus to the Plus spectra */
//0="Pion",1="Kaon",2="Proton",3="Electron",4="Deuteron",5="Triton",6="Helion",7="Alpha",8="Muon"};
void DrawWidthandConf(TString spectrafile, Int_t speciesIndex, Int_t yIndex){

  gSystem->Load("../bin/utilityFunctions_cxx.so");
  gSystem->Load("../bin/ParticleInfo_cxx.so");
  ParticleInfo *particleInfo = new ParticleInfo();

  /** Open File **/
  TFile *f = new TFile(spectrafile, "READ");
  //cout<<f<<endl;

  /** Create Drawing Canvas **/
  TCanvas *c = new TCanvas("c","Draw Width",1);

  /** Get Minus and Plus Spectra **/
  TString speciesName = particleInfo->GetParticleName(speciesIndex);

  TGraphErrors *width = (TGraphErrors*)f->Get(TString::Format("FitParameterizations/widthGraph_%s_%02d",speciesName.Data(),yIndex));
  TGraphErrors *widthConf = (TGraphErrors*)f->Get(TString::Format("FitParameterizations/widthConfGraph_%s_%02d",speciesName.Data(),yIndex));
  widthConf->SetLineColor(kGray);
  widthConf->SetFillColor(kGray);
  widthConf->SetTitle(TString::Format("%s Width Confidence Interval | y= %.03g;m_{T}-m_{0}; Width",speciesName.Data(),GetRapidityRangeCenter(yIndex)));

  widthConf->Draw("A2");
  width->Draw("P SAME");
}
