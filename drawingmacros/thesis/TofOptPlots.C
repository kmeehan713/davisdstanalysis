void TofOptPlots(){

  gStyle->SetOptStat(0);

  gSystem->Load("../../bin/utilityFunctions_cxx.so");
  
  TString pidName = "Pion";
  int yIndex = 20;
  int mTm0Index = 12;
  
  TFile *pidFile = new TFile("../../userfiles/AuAu07/outputFiles/AuAu07_ColliderCenter_pidCalib.root","READ");

  TF1 *pionFit = (TF1 *)pidFile->Get(Form("TOFOptimizedFits_%s/tofOptFit_%s_Pion_yIndex%02d_mTm0Index%02d",
					  pidName.Data(),pidName.Data(),yIndex,mTm0Index));
  TF1 *kaonFit = (TF1 *)pidFile->Get(Form("TOFOptimizedFits_%s/tofOptFit_%s_Kaon_yIndex%02d_mTm0Index%02d",
					  pidName.Data(),pidName.Data(),yIndex,mTm0Index));
  TF1 *protonFit = (TF1 *)pidFile->Get(Form("TOFOptimizedFits_%s/tofOptFit_%s_Proton_yIndex%02d_mTm0Index%02d",
					    pidName.Data(),pidName.Data(),yIndex,mTm0Index));

  pionFit->SetLineWidth(2);
  kaonFit->SetLineWidth(2);
  protonFit->SetLineWidth(2);

  pionFit->SetLineColor(kBlack);
  kaonFit->SetLineColor(kBlack);
  protonFit->SetLineColor(kBlack);

  
  TFile *yieldFile = new TFile("../../userfiles/AuAu07/outputFiles/AuAu07_ColliderCenter_YieldHistos.root","READ");

  TH3D *tofOpt3DPion = (TH3D *)yieldFile->Get(Form("YieldHistograms/zTPC_%s_Pion",pidName.Data()));
  TH3D *tofOpt3DKaon = (TH3D *)yieldFile->Get(Form("YieldHistograms/zTPC_%s_Kaon",pidName.Data()));
  TH3D *tofOpt3DProton = (TH3D *)yieldFile->Get(Form("YieldHistograms/zTPC_%s_Proton",pidName.Data()));

  double rapidityMin = GetRapidityRangeLow(yIndex);
  double rapidityMax = GetRapidityRangeHigh(yIndex);
  double mTm0Min = GetmTm0RangeLow(mTm0Index);
  double mTm0Max = GetmTm0RangeHigh(mTm0Index);
  
  int rapidityBin = tofOpt3DPion->GetXaxis()->FindBin(GetRapidityRangeCenter(yIndex));
  int mTm0Bin   = tofOpt3DPion->GetYaxis()->FindBin(GetmTm0RangeCenter(mTm0Index));
  
  TH1D *tofOptPion = (TH1D *)tofOpt3DPion->ProjectionZ("pion",rapidityBin,rapidityBin,mTm0Bin,mTm0Bin);
  TH1D *tofOptKaon = (TH1D *)tofOpt3DKaon->ProjectionZ("kaon",rapidityBin,rapidityBin,mTm0Bin,mTm0Bin);
  TH1D *tofOptProton = (TH1D *)tofOpt3DProton->ProjectionZ("proton",rapidityBin,rapidityBin,mTm0Bin,mTm0Bin);

  tofOptPion->GetXaxis()->SetRangeUser(-1,2);
  tofOptKaon->GetXaxis()->SetRangeUser(-1,2);
  tofOptProton->GetXaxis()->SetRangeUser(-1,2);

  tofOptPion->GetYaxis()->SetRangeUser(.5,tofOptPion->GetBinContent(tofOptPion->GetMaximumBin())*2);
  tofOptKaon->GetYaxis()->SetRangeUser(.5,tofOptPion->GetBinContent(tofOptPion->GetMaximumBin())*2);
  tofOptProton->GetYaxis()->SetRangeUser(.5,tofOptPion->GetBinContent(tofOptPion->GetMaximumBin())*2);

  tofOptPion->SetLineColor(kBlack);
  tofOptKaon->SetLineColor(kBlack);
  tofOptProton->SetLineColor(kBlack);
  
  tofOptPion->SetMarkerStyle(kFullCircle);
  tofOptKaon->SetMarkerStyle(kFullDiamond);
  tofOptProton->SetMarkerStyle(kFullSquare);

  tofOptPion->SetMarkerSize(1.0);
  tofOptKaon->SetMarkerSize(1.5);
  tofOptProton->SetMarkerSize(1.0);

  tofOptPion->SetMarkerColor(kRed+2);
  tofOptKaon->SetMarkerColor(kBlue);
  tofOptProton->SetMarkerColor(kGreen+2);

  tofOptPion->GetXaxis()->SetTitleFont(63);
  tofOptPion->GetXaxis()->SetTitleSize(20);
  tofOptPion->GetXaxis()->SetTitleOffset(2.75);
  
  tofOptKaon->GetXaxis()->SetTitleFont(63);
  tofOptKaon->GetXaxis()->SetTitleSize(20);
  tofOptKaon->GetXaxis()->SetTitleOffset(2.75);
  
  tofOptProton->GetXaxis()->SetTitleFont(63);
  tofOptProton->GetXaxis()->SetTitleSize(20);
  tofOptProton->GetXaxis()->SetTitleOffset(2.75);

  tofOptPion->SetTitle(";Z_{TPC};dN/dZ_{TPC}");
  tofOptKaon->SetTitle(";Z_{TPC};dN/dZ_{TPC}");
  tofOptProton->SetTitle(";Z_{TPC};dN/dZ_{TPC}");

  tofOptPion->GetYaxis()->SetTitleFont(63);
  tofOptKaon->GetYaxis()->SetTitleFont(63);
  tofOptProton->GetYaxis()->SetTitleFont(63);

  tofOptPion->GetYaxis()->SetTitleSize(20);
  tofOptKaon->GetYaxis()->SetTitleSize(20);
  tofOptProton->GetYaxis()->SetTitleSize(20);

  tofOptPion->GetYaxis()->SetTitleOffset(3.5);
  tofOptKaon->GetYaxis()->SetTitleOffset(3.5);
  tofOptProton->GetYaxis()->SetTitleOffset(3.5);

  tofOptPion->GetYaxis()->SetLabelFont(43);
  tofOptPion->GetYaxis()->SetLabelSize(15);
  tofOptKaon->GetYaxis()->SetLabelFont(43);
  tofOptKaon->GetYaxis()->SetLabelSize(15);
  tofOptProton->GetYaxis()->SetLabelFont(43);
  tofOptProton->GetYaxis()->SetLabelSize(15);

  tofOptPion->GetXaxis()->SetLabelFont(43);
  tofOptPion->GetXaxis()->SetLabelSize(15);
  tofOptKaon->GetXaxis()->SetLabelFont(43);
  tofOptKaon->GetXaxis()->SetLabelSize(15);
  tofOptProton->GetXaxis()->SetLabelFont(43);
  tofOptProton->GetXaxis()->SetLabelSize(15);

  TCanvas *canvas = new TCanvas("canvas","canvas",20,20,450,1080);
  canvas->Divide(1,3);

  TString parSyms[] = {"A","#mu","#sigma","#alpha"};
  
  canvas->cd(1);
  gPad->SetTopMargin(.001);
  gPad->SetRightMargin(.01);
  gPad->SetLeftMargin(.15);
  gPad->SetBottomMargin(.12);
  gPad->SetLogy();
  gPad->SetTicks();
  tofOptPion->Draw("E");
  pionFit->Draw("SAME");
  
  TPaveText *pionSym = new TPaveText(-.75,5000,-.6,5000);
  pionSym->SetFillColor(kWhite);
  pionSym->SetBorderSize(0);
  pionSym->SetTextFont(63);
  pionSym->SetTextSize(40);
  pionSym->AddText("#pi^{#pm}");
  pionSym->Draw("SAME");

  TPaveText *title = new TPaveText(.5,.86,.95,.96,"brNDC");
  title->SetFillColor(kWhite);
  title->SetBorderSize(0);
  title->SetTextFont(63);
  title->SetTextSize(20);
  title->AddText("TOF Optimized Z_{TPC}");
  title->Draw("SAME");

  TPaveText *subTitle = new TPaveText(.52,.64,.96,.87,"brNDC");
  subTitle->SetFillColor(kWhite);
  subTitle->SetBorderSize(0);
  subTitle->SetTextFont(43);
  subTitle->SetTextSize(14);
  subTitle->SetTextAlign(11);
  subTitle->AddText("Au+Au #sqrt{s_{NN}} = 7.7 GeV ");
  subTitle->AddText("Centrality = [0,80]%");
  subTitle->AddText("y_{#pi} = [-0.05,0.05]");
  subTitle->AddText(Form("m_{T}-m_{#pi} = [%.03f,%.03f] (GeV/c^{2})",
			 mTm0Min,mTm0Max));
  subTitle->Draw("SAME");

  TLegend *pionLeg = new TLegend(.656,.533,.954,.632);
  pionLeg->SetFillColor(kWhite);
  pionLeg->SetBorderSize(0);
  pionLeg->SetTextFont(43);
  pionLeg->SetTextSize(14);
  pionLeg->AddEntry(pionFit,"Fit","L");
  pionLeg->Draw("SAME");

 
  TPaveText *pionFitStat = new TPaveText(.654,.294,.951,.549,"brNDC");
  pionFitStat->SetFillColor(kWhite);
  pionFitStat->SetBorderSize();
  pionFitStat->SetTextFont(43);
  pionFitStat->SetTextSize(14);
  pionFitStat->SetTextAlign(11);
  for (int iPar=0; iPar<pionFit->GetNpar(); iPar++){
    pionFitStat->AddText(Form("%s  %.3f #pm %0.3f",parSyms[iPar].Data(),
				pionFit->GetParameter(iPar),
				pionFit->GetParError(iPar)));
  }
  pionFitStat->AddText(Form("#chi^{2}/NDF  %.03g/%d",
			      pionFit->GetChisquare(),pionFit->GetNDF()));
  pionFitStat->GetLine(pionFit->GetNpar())->SetTextSize(12);
  pionFitStat->Draw("SAME");

  
  canvas->cd(2);
  gPad->SetTopMargin(.001);
  gPad->SetRightMargin(.01);
  gPad->SetLeftMargin(.15);
  gPad->SetBottomMargin(.12);
  gPad->SetLogy();
  gPad->SetTicks();
  tofOptKaon->Draw("E");
  kaonFit->Draw("SAME");
  
  TPaveText *kaonSym = new TPaveText(-.75,5000,-.6,5000);
  kaonSym->SetFillColor(kWhite);
  kaonSym->SetBorderSize(0);
  kaonSym->SetTextFont(63);
  kaonSym->SetTextSize(30);
  kaonSym->AddText("K^{#pm}");
  kaonSym->Draw("SAME");

  TLegend *kaonLeg = new TLegend(.205,.641,.5,.740);
  kaonLeg->SetFillColor(kWhite);
  kaonLeg->SetBorderSize(0);
  kaonLeg->SetTextFont(43);
  kaonLeg->SetTextSize(14);
  kaonLeg->AddEntry(kaonFit,"Fit","L");
  kaonLeg->Draw("SAME");

 
  TPaveText *kaonFitStat = new TPaveText(.205,.389,.5,.644,"brNDC");
  kaonFitStat->SetFillColor(kWhite);
  kaonFitStat->SetBorderSize();
  kaonFitStat->SetTextFont(43);
  kaonFitStat->SetTextSize(14);
  kaonFitStat->SetTextAlign(11);
  for (int iPar=0; iPar<kaonFit->GetNpar(); iPar++){
    kaonFitStat->AddText(Form("%s  %.3f #pm %0.3f",parSyms[iPar].Data(),
				kaonFit->GetParameter(iPar),
				kaonFit->GetParError(iPar)));
  }
  kaonFitStat->AddText(Form("#chi^{2}/NDF  %.03g/%d",
			      kaonFit->GetChisquare(),kaonFit->GetNDF()));
  kaonFitStat->GetLine(kaonFit->GetNpar())->SetTextSize(12);
  kaonFitStat->Draw("SAME");

  
  canvas->cd(3);
  gPad->SetTopMargin(.001);
  gPad->SetRightMargin(.01);
  gPad->SetLeftMargin(.15);
  gPad->SetBottomMargin(.12);
  gPad->SetLogy();
  gPad->SetTicks();
  tofOptProton->Draw("E");
  protonFit->Draw("SAME");
  
  TPaveText *protonSym = new TPaveText(-.75,5000,-.6,5000);
  protonSym->SetFillColor(kWhite);
  protonSym->SetBorderSize(0);
  protonSym->SetTextFont(63);
  protonSym->SetTextSize(30);
  protonSym->AddText("p,#bar{p}");
  protonSym->Draw("SAME");

  TLegend *protonLeg = new TLegend(.205,.664,.5,.764);
  protonLeg->SetFillColor(kWhite);
  protonLeg->SetBorderSize(0);
  protonLeg->SetTextFont(43);
  protonLeg->SetTextSize(14);
  protonLeg->AddEntry(protonFit,"Fit","L");
  protonLeg->Draw("SAME");

 
  TPaveText *protonFitStat = new TPaveText(.205,.427,.5,.67,"brNDC");
  protonFitStat->SetFillColor(kWhite);
  protonFitStat->SetBorderSize();
  protonFitStat->SetTextFont(43);
  protonFitStat->SetTextSize(14);
  protonFitStat->SetTextAlign(11);
  for (int iPar=0; iPar<protonFit->GetNpar(); iPar++){
    protonFitStat->AddText(Form("%s  %.3f #pm %0.3f",parSyms[iPar].Data(),
				protonFit->GetParameter(iPar),
				protonFit->GetParError(iPar)));
  }
  protonFitStat->AddText(Form("#chi^{2}/NDF  %.03g/%d",
			      protonFit->GetChisquare(),protonFit->GetNDF()));
  protonFitStat->GetLine(protonFit->GetNpar())->SetTextSize(12);
  protonFitStat->Draw("SAME");

  
}
