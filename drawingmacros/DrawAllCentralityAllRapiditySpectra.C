#include "../inc/globalDefinitions.h"

#include "CanvasPartition.h"

//__________________________________________________________________________
TF1 *funcToScale[9][nRapidityBins];
Double_t scaleFunction(Double_t *x, Double_t *par){

  Double_t xx = x[0];

  Double_t scale = par[0];
  Int_t centIndex = (int)par[1];
  Int_t yIndex = (int)par[2];
  
  return funcToScale[centIndex][yIndex]->Eval(xx) * scale;

}

//___________________________________________________________________________
void RemovePointsWithLargeErrors(TGraphErrors *spectrum, Double_t maxRelativeError=.1){

  //Loop Over the Points of the spectrum. Remove any point which is found
  //to have a relativeError larger than maxRelativeError
  for (int iPoint=spectrum->GetN()-1; iPoint>=0; iPoint--){

    if (spectrum->GetEY()[iPoint] / spectrum->GetY()[iPoint] > maxRelativeError)
      spectrum->RemovePoint(iPoint);
  }

}

//___________________________________________________________________________
void DrawAllCentralityAllRapiditySpectra(TString spectraFile, TString eventConfig, TString system,
					 Double_t energy, Int_t speciesIndex, Int_t charge,
					 Double_t midY, Bool_t corrected=false){

  Bool_t save = true;        //Should the canvas be saved?
  Double_t scaleFactor = 2.5; //The factor to scale the non- midrapidity spectra by
  Int_t yMin = 11;            //Minimum rapidity bin index to draw
  Int_t yMax = 29;            //Maximum rapidity bin index to draw
  Double_t maxmTm0=1.5;       //Maximum mT-m0 value to draw

  Int_t nPadsX(9);
  Int_t nPadsY(1);

  gSystem->Load("../bin/utilityFunctions_cxx.so");
  gSystem->Load("../bin/TrackInfo_cxx.so");
  gSystem->Load("../bin/PrimaryVertexInfo_cxx.so");
  gSystem->Load("../bin/EventInfo_cxx.so");
  gSystem->Load("../bin/ParticleInfo_cxx.so");
  gSystem->Load("../bin/StRefMultExtendedCorr_cxx.so");
  gSystem->Load("../bin/UserCuts_cxx.so");
  SetVariableUserCuts(energy,eventConfig,"");
  ParticleInfo *particleInfo = new ParticleInfo();

  yMin = GetMinRapidityIndexOfInterest(speciesIndex);
  yMax = GetMaxRapidityIndexOfInterest(speciesIndex);

  Double_t yScaleMin(0), yScaleMax(0);
  if (speciesIndex == PION){
    yScaleMin = .0000025;
    yScaleMax = 2000000;
  }
  else if (speciesIndex == PROTON){
    yScaleMin = .00000025;
    yScaleMax = 30000;
  }
  else if (speciesIndex == KAON){
    yScaleMin = .00000025;
    yScaleMax = 3000000;
  }
  
  //Load the root file
  TFile *file = new TFile(spectraFile,"READ");

  //Create the Spectra Name
  TString type = "raw";
  TString Type = "Raw";
  TString name = "Uncorrected";
  if (corrected){
    type = "corrected";
    Type = "Corrected";
    name = "Corrected";
  }

  Int_t midRapidityIndex = GetRapidityIndex(midY);
  const int nCentralityBins = GetNCentralityBins();
  
  //Create the Canvas
  TCanvas *canvas = new TCanvas(Form("%s%02d_%s_%s",system.Data(),(int)energy,
				     particleInfo->GetParticleName(speciesIndex,charge).Data(),eventConfig.Data()),
				"",5,20,210*nPadsX,400*nPadsY);  
  
  CanvasPartition(canvas,nPadsX,nPadsY);

  canvas->cd(1);
  TPaveText *starPrelim = new TPaveText(.48,.12,.96,.19,"NDC");
  starPrelim->SetFillColor(kWhite);
  starPrelim->SetFillStyle(0);
  starPrelim->SetBorderSize(0);
  starPrelim->SetTextFont(63);
  starPrelim->SetTextSize(8);
  starPrelim->AddText("STAR PRELIMINARY");
  
  //Loop Over the Pads and Draw
  for (int iPadX=0; iPadX<nPadsX; iPadX++){
    for (int iPadY=0; iPadY<nPadsY; iPadY++){

      int iCentBin = iPadX;
      TString speciesName = particleInfo->GetParticleName(speciesIndex,charge);
      
      gPad = (TPad *)gROOT->FindObject(Form("pad_%i_%i",iPadX,iPadY));
      gPad->SetLogy();
      gPad->SetTicks(1,1);
      TH1F *frame = gPad->DrawFrame(-0.05,yScaleMin,maxmTm0+.2,yScaleMax);
      frame->GetXaxis()->SetTitle(Form("m_{T}-m_{%s} (GeV/c^{2})",
				       particleInfo->GetParticleSymbol(speciesIndex).Data()));

      if (iPadX==0){

	//Axis Titles
	frame->GetYaxis()->SetTitle("#frac{1}{N_{Evt}}#times#frac{1}{2#pim_{T}}#times#frac{d^{2}N}{dm_{T}dy} (GeV/c^{2})^{2}");
	frame->GetYaxis()->SetTitleOffset(2.25);
	frame->GetYaxis()->SetLabelFont(63);
	frame->GetYaxis()->SetLabelSize(14);
	frame->GetYaxis()->SetTitleFont(63);
	frame->GetYaxis()->SetTitleSize(16);

	//Row Title
	TPaveText *title = new TPaveText(.42,.86,.95,.975,"NBNDCBR");
	title->SetFillColor(kWhite);
	title->SetBorderSize(0);
	title->SetTextFont(63);
	title->SetTextSize(18);
	title->SetTextAlign(32);
	title->AddText(Form("%s Spectra %s",
			    particleInfo->GetParticleSymbol(speciesIndex,charge).Data(),
			    system.Data()));
	title->AddText(Form("#sqrt{s_{NN}} = %.03g GeV",energy));
	title->Draw("SAME");

	TMarker *midYMarker = new TMarker(0,0,kFullCircle);
	midYMarker->SetMarkerColor(kRed);
	
	TMarker *notMidYMarker = new TMarker(0,0,kFullCircle);
	notMidYMarker->SetMarkerColor(kBlack);
	
	TLegend *leg = new TLegend(.55,.76,.80,.85);
	leg->SetLineColor(kWhite);
	leg->SetBorderSize(0);
	leg->SetFillColor(kWhite);
	leg->SetTextFont(63);
	leg->SetTextSize(14);
	leg->AddEntry(midYMarker,"Mid Rapidity","P");
	leg->AddEntry(notMidYMarker,Form("x %g^{#pm n}",scaleFactor),"P");
	leg->Draw("SAME");
	
      }

      //X Axis Title
      frame->GetXaxis()->SetLabelFont(43);
      frame->GetXaxis()->SetLabelSize(14);
      frame->GetXaxis()->SetTitleFont(63);
      frame->GetXaxis()->SetTitleSize(16);
      frame->GetXaxis()->SetNdivisions(504);

      //Centrality Title
      TPaveText *centTitle = new TPaveText(iPadX==0 ? .28:.05,.12,
					   iPadX==0 ? .52:.40,.19,"NDC");
      centTitle->SetFillColor(kWhite);
      centTitle->SetBorderSize(0);
      centTitle->SetTextFont(63);
      centTitle->SetTextSize(18);
      centTitle->SetTextAlign(12);
      centTitle->AddText(Form("%d-%d%%",
			      iCentBin!=nCentralityBins-1 ? (int)GetCentralityPercents().at(iCentBin+1):0,
			      (int)GetCentralityPercents().at(iCentBin)));
      //centTitle->AddText("Central");
      centTitle->Draw("SAME");

      //Draw the Preliminary Text
      starPrelim->Draw("SAME");
      
      //Draw the Spectra
      for (int yIndex=yMin; yIndex<=yMax; yIndex++){

	TGraphErrors *spectra = NULL;
	TGraphErrors *scaledSpectra = NULL;
	TGraphErrors *spectraTOF = NULL;
	TGraphErrors *scaledSpectraTOF = NULL;
	TF1 *spectraFit = NULL;
	TF1 *scaledSpectraFit = NULL;

	TString spectraName(Form("%sSpectra_%s_Cent%02d_yIndex%02d",
				 type.Data(),speciesName.Data(),iCentBin,yIndex));
	TString spectraNameTOF(Form("%sSpectraTOF_%s_Cent%02d_yIndex%02d",
				    type.Data(),speciesName.Data(),iCentBin,yIndex));
	TString spectraFitName(Form("finalSpectrum_%s_Cent%02d_yIndex%02d_Fit",
				    speciesName.Data(),iCentBin,yIndex));

	spectra = (TGraphErrors *)file->Get(Form("%sSpectra_%s/%s",
						 Type.Data(),speciesName.Data(),
						 spectraName.Data()));
	spectraTOF = (TGraphErrors *)file->Get(Form("%sSpectra_%s/%s",
						    Type.Data(),speciesName.Data(),
						    spectraNameTOF.Data()));
	spectraFit = (TF1 *)file->Get(Form("SpectraFits_%s/%s",
					   speciesName.Data(),
					   spectraFitName.Data()));

	//Skip if no spectrum was found for this rapidity bin
	if (spectra == NULL)
	  continue;
	
	//Make sure spectra have points
	if (spectra->GetN() == 0){
	  cout <<"TPC spectrum has no points! Skipping this rapidity bin." <<endl;
	  delete spectra;
	  continue;
	}
	if (spectraTOF){
	  if (spectraTOF->GetN() == 0){
	    cout <<"TOF spectrum has no points! TOF spectrum will not be used." <<endl;
	    delete spectraTOF;
	    spectraTOF = NULL;
	  }
	}
	
	//Remove high mTm0 Point
	TGraphChop(spectra,maxmTm0,false);
	if (spectraTOF)
	  TGraphChop(spectraTOF,maxmTm0,false);

	//Remove Point With large errors
	RemovePointsWithLargeErrors(spectra);
	if (spectraTOF)
	  RemovePointsWithLargeErrors(spectraTOF);
	
	//Scale the Spectra
	scaledSpectra = TGraphScale(spectra,pow(scaleFactor,yIndex-midRapidityIndex));
	if (spectraTOF)
	  scaledSpectraTOF = TGraphScale(spectraTOF,pow(scaleFactor,yIndex-midRapidityIndex));

	//Scale the Spectra Fit
	Double_t fitMin(0), fitMax(0);
	if (spectraFit){
	  funcToScale[iCentBin][yIndex] = spectraFit;
	  spectraFit->GetRange(fitMin,fitMax);
	  scaledSpectraFit = new TF1(Form("%s_Scaled",spectraFit->GetName()),
				     scaleFunction,fitMin,maxmTm0,3);
	  scaledSpectraFit->SetParameter(0,pow(scaleFactor,yIndex-midRapidityIndex));
	  scaledSpectraFit->SetParameter(1,iCentBin);
	  scaledSpectraFit->SetParameter(2,yIndex);
	}
	
	//Set the Mid Rapidity Spectrum to Red
	if (yIndex == midRapidityIndex){
	  scaledSpectra->SetMarkerColor(kRed);
	  //	  label->SetTextColor(kRed);
	  if (spectraTOF && scaledSpectraTOF)
	    scaledSpectraTOF->SetMarkerColor(kRed);
	  if (scaledSpectraFit)
	    scaledSpectraFit->SetLineColor(kRed);
	}
	else{
	  scaledSpectra->SetMarkerColor(kBlack);
	  if (spectraTOF && scaledSpectraTOF)
	    scaledSpectraTOF->SetMarkerColor(kBlack);
	  if (scaledSpectraFit)
	    scaledSpectraFit->SetLineColor(kBlack);
	}

	scaledSpectra->SetMarkerStyle(kFullDotLarge);
	scaledSpectra->SetMarkerSize(0.7);
	if (scaledSpectraTOF){
	  scaledSpectraTOF->SetMarkerStyle(kFullDotLarge);
	  scaledSpectraTOF->SetMarkerSize(0.7);
	}
	
	scaledSpectra->Draw("PZ");
	if (spectraTOF && scaledSpectraTOF)
	  scaledSpectraTOF->Draw("PZ");

	if (scaledSpectraFit){
	  scaledSpectraFit->Draw("SAME");
	}
	
      }
      
      gPad->Modified();
      gPad->Update();
      
    }//End Loop Over yPads
  }//End Loop Over xPads

  canvas->Modified();
  canvas->Update();

  if (save){
    canvas->Print(Form("%s.gif",canvas->GetName()));
  }

  
}




