//This macro runs the compiled RunByRunDetectorPerformance QA binary.
//The source code can be found at
//   src/qa/RunByRunDetectorPerformance.cxx

void RunDetectorPerformanceQA(TString inputFile, TString starLibrary, Double_t energy,
			      TString eventConfig, TString outputFile=""){
  
  //Load the Necessary Libraries
  gSystem->Load("../bin/TrackInfo_cxx.so");
  gSystem->Load("../bin/PrimaryVertexInfo_cxx.so");
  gSystem->Load("../bin/EventInfo_cxx.so");
  gSystem->Load("../bin/ParticleInfo_cxx.so");

  gSystem->Load("../bin/utilityFunctions_cxx.so");
  gSystem->Load("../bin/usefulDataStructs_cxx.so");
  gSystem->Load("../bin/DavisDstReader_cxx.so");
  gSystem->Load("../bin/StRefMultExtendedCorr_cxx.so");
  gSystem->Load("../bin/UserCuts_cxx.so");
  gSystem->Load("../bin/RunByRunDetectorPerformance_cxx.so");

  //Set the User Cuts
  SetVariableUserCuts(energy,eventConfig,starLibrary);

  RunByRunDetectorPerformance(inputFile,outputFile);

}
