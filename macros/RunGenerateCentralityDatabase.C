//This macro runs the GenerateCentralityDatabase binary.
//The source code can be foudn at src/embedding/GenerateCentralityDatabase.cxx

void RunGenerateCentralityDatabase(TString inputFile, TString outputFile,
				   TString starLibrary, Double_t energy,
				   TString eventConfig){

  //Load the Necessary Libraries
  gSystem->Load("../bin/TrackInfo_cxx.so");
  gSystem->Load("../bin/PrimaryVertexInfo_cxx.so");
  gSystem->Load("../bin/EventInfo_cxx.so");
  gSystem->Load("../bin/DavisDstReader_cxx.so");
  gSystem->Load("../bin/StRefMultExtendedCorr_cxx.so");
  gSystem->Load("../bin/UserCuts_cxx.so");
  gSystem->Load("../bin/CentralityDatabaseClass_cxx.so");
  gSystem->Load("../bin/GenerateCentralityDatabase_cxx.so");
  
  //Set the Variable User Cuts
  SetVariableUserCuts(energy,eventConfig,starLibrary);

  GenerateCentralityDatabase(inputFile,outputFile);  

}
